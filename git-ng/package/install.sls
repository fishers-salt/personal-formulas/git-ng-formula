# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as git_ng with context %}

git-ng-package-install-pkg-installed:
  pkg.installed:
    - name: {{ git_ng.pkg.name }}
