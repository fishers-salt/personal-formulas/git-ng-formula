# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as git_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('git-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_git-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

git-ng-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}
    - remove_groups: False

git-ng-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/git
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - git-ng-config-file-user-{{ name }}-present

{% set fullname = user.get('fullname', None) %}
{% set email = user.get('email', None) %}
git-ng-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/git/config
    - source: {{ files_switch([
                   name ~ '-gitconfig.tmpl', 'gitconfig.tmpl'],
                 lookup='git-ng-config-file-config-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
        fullname: {{ fullname }}
        email: {{ email }}
    - require:
      - git-ng-config-file-config-dir-{{ name }}-managed

git-ng-config-file-ignore-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/git/gitignore
    - source: {{ files_switch([
                   name ~ '-gitignore.tmpl', 'gitignore.tmpl'],
                 lookup='git-ng-config-file-ignore-config-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - git-ng-config-file-config-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
