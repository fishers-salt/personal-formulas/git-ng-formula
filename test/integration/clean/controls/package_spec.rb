# frozen_string_literal: true

control 'git-ng-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('git') do
    it { should_not be_installed }
  end
end
