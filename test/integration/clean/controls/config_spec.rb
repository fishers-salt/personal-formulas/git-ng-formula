# frozen_string_literal: true

control 'git-ng-config-clean-ignore-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/git/gitignore') do
    it { should_not exist }
  end
end

control 'git-ng-config-clean-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/git/config') do
    it { should_not exist }
  end
end

control 'git-ng-config-clean-git-ng-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/git') do
    it { should_not exist }
  end
end
