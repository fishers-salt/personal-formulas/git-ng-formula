# frozen_string_literal: true

control 'git-ng-package-install-pkg-installed' do
  title 'should be installed'

  describe package('git') do
    it { should be_installed }
  end
end
