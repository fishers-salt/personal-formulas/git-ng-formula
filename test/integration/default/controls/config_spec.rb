# frozen_string_literal: true

control 'git-ng-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'git-ng-config-file-git-ng-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/git') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'git-ng-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/git/config') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('name = josephus miller') }
    its('content') { should include('email = jmiller@ceres.station') }
  end
end

control 'git-ng-config-file-ignore-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/git/gitignore') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Project files') }
    its('content') { should include('# asdf files') }
  end
end
